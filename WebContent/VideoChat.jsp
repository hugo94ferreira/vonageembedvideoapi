<%@ page language="java" 
	     contentType="text/html; charset=ISO-8859-1" 
	     pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.UUID" %>

<!-- 
#### @AUTHOR HugoFerreira 
-->

<html>

<head>
<title>Video Chat</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="css/videoIframe.css" rel="stylesheet">
	<script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
</head>

<body>

	<!-- 
	### VONAGE IFRAME PARAMS  
	-->
		
	<%String embedId = (String) session.getAttribute("embedId");%>
	<%String uuid = (String) session.getAttribute("roomId");%>
	
	<iframe
		class="responsive-iframe" 
		src="https://tokbox.com/embed/embed/ot-embed.js?embedId=<%=embedId%>&room=<%=uuid%>&iframe=true"
		width="800px" 
		height="640px" 
		scrolling="auto"
		allow="microphone; camera">
	</iframe>
	
</body>

</html>