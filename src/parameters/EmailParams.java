package parameters;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class EmailParams {
	
	private String paramsLocation = "";
	private String receiverEmail = "";
	private String senderEmail = "";
	private String subject = "";
	private String username = "";
	private String password = "";
	private String isTls = "";
	private String isAuth = "";
	private String hostSMTP = "";
	private String portSMTP = "";
	
	public EmailParams(){}
	public EmailParams(String fileLocation){
		loademailParams(fileLocation);
	}
	
	/**
	 * @param fileLocation
	 * @return emailParams
	 */
	private EmailParams loademailParams(String fileLocation){
				
		EmailParams emailParams = this;
		try{
			
			File file = new File(fileLocation + File.separator + "emailParams.xml");
			JAXBContext jaxbContext = JAXBContext.newInstance(emailParams.getClass());

			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			emailParams = (EmailParams) jaxbUnmarshaller.unmarshal(file);

			paramsLocation = emailParams.getParamsLocation();	
			receiverEmail = emailParams.getReceiverEmail();
			senderEmail = emailParams.getSenderEmail();
			subject = emailParams.getSubject();
			username = emailParams.getUsername();
			password = emailParams.getPassword();
			isTls = emailParams.getIsTls();
			isAuth = emailParams.getIsAuth();
			hostSMTP = emailParams.getHostSMTP();
			portSMTP = emailParams.getPortSMTP();
			
		}catch(JAXBException ex){
			
		}
		
		return emailParams;
	}
	
	@Override
	public String toString(){
		
		return new StringBuilder()
			 	   .append(" | ReceiverEmail: " + receiverEmail)
			 	   .append(" | SenderEmail: " + senderEmail)
			 	   .append(" | Username: " + username)
			 	   .append(" | Password: " + password)
			 	   .append(" | IsTls: " + isTls)
			 	   .append(" | IsAuth: " + isAuth)
			 	   .append(" | HostSMTP: " + hostSMTP)
			 	   .append(" | PortSMTP: " + portSMTP)
			 	   .toString();
	}
	
	public String getParamsLocation() {
		return paramsLocation;
	}
	@XmlElement
	public void setParamsLocation(String paramsLocation) {
		this.paramsLocation = paramsLocation;
	}
	public String getReceiverEmail() {
		return receiverEmail;
	}
	@XmlElement
	public void setReceiverEmail(String receiverEmail) {
		this.receiverEmail = receiverEmail;
	}
	public String getSenderEmail() {
		return senderEmail;
	}
	@XmlElement
	public void setSenderEmail(String senderEmail) {
		this.senderEmail = senderEmail;
	}
	public String getSubject() {
		return subject;
	}
	@XmlElement
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getUsername() {
		return username;
	}
	@XmlElement
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	@XmlElement
	public void setPassword(String password) {
		this.password = password;
	}
	public String getIsTls() {
		return isTls;
	}
	@XmlElement
	public void setIsTls(String isTls) {
		this.isTls = isTls;
	}
	public String getIsAuth() {
		return isAuth;
	}
	@XmlElement
	public void setIsAuth(String isAuth) {
		this.isAuth = isAuth;
	}
	public String getHostSMTP() {
		return hostSMTP;
	}
	@XmlElement
	public void setHostSMTP(String hostSMTP) {
		this.hostSMTP = hostSMTP;
	}
	public String getPortSMTP() {
		return portSMTP;
	}
	@XmlElement
	public void setPortSMTP(String portSMTP) {
		this.portSMTP = portSMTP;
	}
}
