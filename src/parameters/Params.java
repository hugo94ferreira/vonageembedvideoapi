package parameters;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Params {

	private String paramsLocation = "";
	private String embedId = "";
	private String link = "";

	public Params(){}
	public Params(String fileLocation){
		loadParams(fileLocation);
	}
	
	/**
	 * @param fileLocation
	 * @return params
	 */
	private Params loadParams(String fileLocation){
				
		Params params = this;
		try{
			
			File file = new File(fileLocation + File.separator + "apiParams.xml");
			JAXBContext jaxbContext = JAXBContext.newInstance(params.getClass());

			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			params = (Params) jaxbUnmarshaller.unmarshal(file);

			paramsLocation = params.getParamsLocation();		
			embedId = params.getEmbedId();
			link = params.getLink();
			
		}catch(JAXBException ex){
			
		}
		
		return params;
	}
	
	@Override
	public String toString(){
		
		return new StringBuilder()
			 	   .append(" | EmbedId: " + embedId)
			 	   .append(" | Link: " + link)
			 	   .toString();
	}
	
	public String getParamsLocation() {
		return paramsLocation;
	}
	@XmlElement
	public void setParamsLocation(String paramsLocation) {
		this.paramsLocation = paramsLocation;
	}
	public String getEmbedId() {
		return embedId;
	}
	@XmlElement
	public void setEmbedId(String embedId) {
		this.embedId = embedId;
	}	
	public String getLink() {
		return link;
	}
	@XmlElement
	public void setLink(String link) {
		this.link = link;
	}	
}
