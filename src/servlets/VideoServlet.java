package servlets;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import parameters.EmailParams;
import parameters.Params;
import via.mail.parameters.MailParameters;
import via.mail.sender.ViaMail;

/*** @author HugoFerreira */
@WebServlet("VideoServlet")
public class VideoServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger("VideoChat");
	private static final String CLASS_NAME = VideoServlet.class.getSimpleName();

	private Params apiParams;
	private EmailParams emailParams;

	String paramsLocation = "";
	String log4jFile = "";

	public VideoServlet() {
		super();
	}

	@Override
	public void init() throws ServletException {

		paramsLocation = System.getProperty("video.configs");

		if (paramsLocation == null || paramsLocation.trim().isEmpty()) {
			throw new RuntimeException("System property video.configs does not exists!");
		}

		log4jFile = System.getProperty("video.configs") + File.separator + "log4j.properties";
		PropertyConfigurator.configure(log4jFile);
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.trace("[" + CLASS_NAME + "]" + "DoGet START");

		HttpSession session = request.getSession(true);
		String roomId = "";
		String email = "&email=true";

		apiParams = new Params(paramsLocation);

		String sQueryParams = request.getQueryString() == null ? "" : request.getQueryString();
		logger.trace("[" + CLASS_NAME + "]" + " sQueryParams: " + sQueryParams);

		if (!sQueryParams.trim().isEmpty() && sQueryParams.contains(email)) {			
			
			roomId = sQueryParams.split("&")[0].split("=")[1];
		} else {
			roomId = UUID.randomUUID().toString().replace("-", "");
			
			String linkToMail = apiParams.getLink() + roomId + email;
			logger.trace("[" + CLASS_NAME + "]" + " linkToMail: " + linkToMail);

			sendEmail(linkToMail, roomId);
		}

		session.setAttribute("embedId", apiParams.getEmbedId());
		logger.trace("[" + CLASS_NAME + "]" + " embedId: " + apiParams.getEmbedId());
		session.setAttribute("roomId", roomId);
		logger.trace("[" + CLASS_NAME + "]" + " roomId: " + roomId);

		logger.trace("[" + CLASS_NAME + "]" + "DoGet END");

		response.sendRedirect("VideoChat.jsp");

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.trace("[" + CLASS_NAME + "]" + "DoPost START");
		logger.trace("[" + CLASS_NAME + "]" + "DoPost END");
	}

	/**
	 * <b>Send Email with generated link</b> 
	 * <b>User will be able to
	 * join the chat through the link</b>
	 * 
	 * @param link
	 * @param uuid
	 */
	public void sendEmail(String link, String uuid) {

		try {
			emailParams = new EmailParams(paramsLocation);

			String username = emailParams.getUsername();
			logger.trace("[" + CLASS_NAME + "]" + " username: " + username);
			String password = emailParams.getPassword();
			logger.trace("[" + CLASS_NAME + "]" + " password: " + password);
			String receiverEmail = emailParams.getReceiverEmail();
			logger.trace("[" + CLASS_NAME + "]" + " receiverEmail: " + receiverEmail);
			String senderEmail = emailParams.getSenderEmail();
			logger.trace("[" + CLASS_NAME + "]" + " senderEmail: " + senderEmail);
			String subject = emailParams.getSubject();
			logger.trace("[" + CLASS_NAME + "]" + " subject: " + subject);
			String isTls = emailParams.getIsTls();
			logger.trace("[" + CLASS_NAME + "]" + " isTls: " + isTls);
			String isAuth = emailParams.getIsAuth();
			logger.trace("[" + CLASS_NAME + "]" + " isAuth: " + isAuth);
			String hostSMTP = emailParams.getHostSMTP();
			logger.trace("[" + CLASS_NAME + "]" + " hostSMTP: " + hostSMTP);
			String portSMTP = emailParams.getPortSMTP();
			logger.trace("[" + CLASS_NAME + "]" + " portSMTP: " + portSMTP);

			MailParameters mailParameters = new MailParameters();
			mailParameters.setSenderUser(username);
			mailParameters.setSenderPassword(password);
			mailParameters.setSmtpStartTtlsEnabe(isTls);
			mailParameters.setSmtpAuth(isAuth);
			mailParameters.setSmtpHost(hostSMTP);
			mailParameters.setSmtpPort(portSMTP);
			mailParameters.setSenderMail(senderEmail);
			mailParameters.setSubject(subject);
			mailParameters.setTextoMail(link);

			ViaMail sendEmail = new ViaMail(mailParameters, uuid);

			sendEmail.sendSimpleMail(receiverEmail);

		} catch (Exception e) {
			// TODO: handle exception
			logger.error("[" + CLASS_NAME + "]" + " Exception: " + e);
		}
	}

}
 