*****************************************

VONAGE EMBED VIDEO API

The Video Chat Embed is the easiest way to quickly add basic OpenTok functionality to your website using an embeddable HTML snippet.
Once implemented on your site, you can click the embed to connect to a "video chat room." 
Other users can then click on the embed in their own browser and join the same chat room. 
You can even create separate rooms with the same embed using the room parameter.

*****************************************

This integration consist in create a link(1) that is going open a video chat room, same link is therefore sent via email(2) to someone we want to join us in the same room.